package VegetableFactory;

/**
 *
 * @author akret
 */
public enum VegetableType {
    BEET,
    CARROT
}
