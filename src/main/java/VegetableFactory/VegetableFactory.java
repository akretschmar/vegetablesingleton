
package VegetableFactory;

/**
 *
 * @author akret
 */
public class VegetableFactory {
    public static VegetableFactory instance;
    
    private VegetableFactory(){}
    
    
    public static VegetableFactory getInstance(){
        if (instance==null){
        instance = new VegetableFactory();
        }           
        return instance;
    }
    public Vegetable getVegetable(VegetableType type, String colour, double size){
        switch( type ){
            case CARROT : return new Carrot( colour, size);
            case BEET : return new Beet(colour, size);
            
        }
        return null;
    }
}
