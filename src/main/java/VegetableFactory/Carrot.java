package VegetableFactory;

/**
 *
 * @author akret
 */
public class Carrot extends Vegetable {
    
    
public Carrot(){}

public Carrot(String colour, double size){
    super(colour, size);
    
}

@Override
public boolean isRipe(){  
    return getSize() >= 1.5 && getColour().equalsIgnoreCase("orange");
}

    @Override
    public String toString() {
        String out = "--------------------\n";        
        out += "Colour: " + colour + "\n";
        out += "Size: " + size + "\n";
        out += "Ripe: " + isRipe() + "\n";
        out += "--------------------\n";
        
        return out;
    }
}

