
package VegetableFactory;

/**
 *
 * @author akret
 */
public class Beet extends Vegetable{
   
    
public Beet(){}
    
public Beet(String colour, double size){
    super(colour, size);
    
}

@Override
boolean isRipe(){
   return getSize() >= 2.0 && getColour().equalsIgnoreCase("red");
}

    @Override
    public String toString() {
        String out = "--------------------\n";      
        out += "Colour: " + colour + "\n";
        out += "Size: " + size + "\n";
        out += "Ripe: " + isRipe()+ "\n";
        out += "--------------------\n";
        
        return out;
    }




}

