package VegetableFactory;

/**
 *
 * @author akret
 */
public class VegetableDemo {

    public static void main(String[] args) {
        VegetableFactory factory = VegetableFactory.getInstance();
        
        Vegetable carrot = factory.getVegetable( VegetableType.CARROT, "ORANGE", 2);
        Vegetable carrot2 = factory.getVegetable( VegetableType.CARROT, "YELLOW", 2);
        
        Vegetable beet = factory.getVegetable( VegetableType.BEET, "RED", 2);
        Vegetable beet2 = factory.getVegetable( VegetableType.BEET, "RED", 1.5);
        
        System.out.println("colour of " + carrot.getClass().getCanonicalName()+ " is " + carrot.getColour() + " and is it ripe? " + carrot.isRipe());
        System.out.println("colour of " + carrot2.getClass().getCanonicalName()+ " is " + carrot2.getColour() + " and is it ripe? " + carrot2.isRipe());
        
        System.out.println("colour of " + beet.getClass().getCanonicalName()+ " is " + beet.getColour() + " and is it ripe? " + beet.isRipe());
        System.out.println("colour of " + beet2.getClass().getCanonicalName()+ " is " + beet2.getColour() + " and is it ripe? " + beet2.isRipe());
    
    }
    
}
